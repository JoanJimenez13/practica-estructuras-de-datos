

#AÑADIENDO DATOS A LA LISTA
print ("AÑADIENDO DATOS A LA LISTA")
lista = ["A","B","C","D"]
lista.append("E")
print(lista)

#MODIFICANDO DATOS DENTRO DE LA LISTA
print ("MODIFICANDO DATOS DENTRO DE LA LISTA")
lista[1] = "H"
lista[2] = "K"
print(lista)

#BORRANDO DATOS DENTRO DE LA LISTA
print ("BORRANDO DATOS DENTRO DE LA LISTA")
lista.remove("E")
lista.remove("H")
print(lista)


#BUSCANDO DATOS DENTRO DE LA LISTA 
print ("BUSCANDO DATOS DENTRO DE LA LISTA")
print (lista.index("K"))
print (lista.index("A"))