class Stack_Pila:
	"""docstring for ClassName"""
	def __init__(self):
		self.Top = None
		self.Stack = []


	def GetTop(self):
		print (self.Top)


	def add(self,Elemento):
		self.Top = Elemento
		self.Stack.append(Elemento)


	def Remove(self):
		self.Stack.pop()
		self.Top = self.Stack[-1]

	def _str_(self):
		print (str(self.Stack))
		
	def _len_(self):
		print (len (self.Stack))


#AÑADIENDO DATOS A LA PILA
MyStack = Stack_Pila()
MyStack.add(1)
MyStack.add(2)
MyStack.add(3)
MyStack.add(4)
MyStack.add(5)
print("ELEMENTOS DE LA PILA: ")
print (MyStack.Stack)


#OBTENIENDO EL ELEMENTO TOP O INICIAL DE LA PILA
print("EL ELEMENTO TOP O INICIAL DE LA PILA ES: ")
MyStack.GetTop()

#REMOVIENDO DATOS DE LA PILA
print("REMOVIENDO DATOS DE LA PILA... ")
MyStack.Remove()
MyStack.Remove()
print("DATOS DE LA PILA ACTUALMENTE: ")
print (MyStack.Stack)


#BUSCANDO EL INDICE DEL ELEMENTO DENTRO DE LA PILA 
print ("BUSCANDO EL INDICE DEL ELEMENTO DENTRO DE LA PILA: ")
print (MyStack.Stack.index(3))
