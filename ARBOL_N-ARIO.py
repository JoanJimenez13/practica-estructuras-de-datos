# Arbol N-ario de productos electronicos
class NodoArbol:
    # constructor
    def __init__(self, dato):
        self.dato = dato
        self.hijos = []
        self.padre = None

    def agregar_hijos(self, hijo):
        hijo.padre = self
        self.hijos.append(hijo)

    def obtener_nivel(self):
        nivel = 0
        p = self.padre
        while p:
            nivel += 1
            p = p.padre

        return nivel

    def imprimir_arbol(self, nivel):
        if self.obtener_nivel() > nivel:
            return

        espacios = ' ' * self.obtener_nivel() * 3
        prefijo = espacios + "|__" if self.padre else ""

        print(prefijo + self.dato)
        if len(self.hijos) > 0:
            for hijo in self.hijos:
                hijo.imprimir_arbol(nivel)


def crear_arbol():
    raiz = NodoArbol("Electrónicos")

    laptop = NodoArbol("Laptop")
    laptop.agregar_hijos(NodoArbol("Mac"))
    laptop.agregar_hijos(NodoArbol("Surface"))
    laptop.agregar_hijos(NodoArbol("ThinkPad"))

    celulares = NodoArbol("celulares")
    celulares.agregar_hijos(NodoArbol("Iphone"))
    celulares.agregar_hijos(NodoArbol("Google Pixel"))
    celulares.agregar_hijos(NodoArbol("Xiaomi"))

    tv = NodoArbol("TV")
    tv.agregar_hijos(NodoArbol("Samsung"))
    tv.agregar_hijos(NodoArbol("LG"))

    raiz.agregar_hijos(laptop)
    raiz.agregar_hijos(celulares)
    raiz.agregar_hijos(tv)

    return raiz


# main
if __name__ == '__main__':
    nodo_raiz = crear_arbol()
    nodo_raiz.imprimir_arbol(2)
