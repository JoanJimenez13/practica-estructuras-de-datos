# clase para definir los vertices de los grafos
class Vertice:
    # Método que inicializa el vértice con sus atributos
    def __init__(self, id):
        self.id = id
        self.visitado = False
        self.nivel = -1
        self.padre = None
        self.vecinos = []
        self.distancia = float('inf')

    def agregarVecino(self, v, p):
        if v not in self.vecinos:
            self.vecinos.append([v, p])


class Grafo:
    def __init__(self):
        self.vertices = {}

    def agregarVertice(self, v):
        if v not in self.vertices:
            self.vertices[v] = Vertice(v)

    def agregarArista(self, a, b, p):
        if a in self.vertices and b in self.vertices:
            self.vertices[a].agregarVecino(b, p)
            self.vertices[b].agregarVecino(a, p)

    def imprimirGrafo(self):
        for v in self.vertices:
            print("La distancia del vertice "+str(v)+" es " +
                  str(self.vertices[v].distancia)+" llegando desde "+str(self.vertices[v].padre))

    def camino(self, a, b):
        """Método que va guardando en la lista llamada 'camino' los nodos en el orden que sean visitados y actualizando dicha
        lista con los vértices con la menor distancia"""
        camino = []
        actual = b
        while actual != None:
            camino.insert(0, actual)
            actual = self.vertices[actual].padre
        return [camino, self.vertices[b].distancia]

    def minimo(self, lista):
        """Método que recibe la lista de los vertices no visitados, revisa si su longitud es mayor a cero(indica que 
        aún hay vértices sin visitar), y realiza comparaciones de las distancias de cada vértice en ésta lista para encontrar
        el de menor distancia"""
        if len(lista) > 0:
            m = self.vertices[lista[0]].distancia
            v = lista[0]
            for e in lista:
                if m > self.vertices[e].distancia:
                    m = self.vertices[e].distancia
                    v = e

            return v

    def dijkstra(self, a):
        """Método que sigue el algoritmo de Dijkstra
        1. Asignar a cada nodo una distancia tentativa: 0 para el nodo inicial e infinito para todos los nodos restantes. Predecesor nulo para todos.
        2. Establecer al nodo inicial como nodo actual y crear un conjunto de nodos no visitados.
        3. Para el nodo actual, considerar a todos sus vecinos no visitados con peso w.
                a) Si la distancia del nodo actual sumada al peso w es menor que la distancia tentativa actual de ese vecino, sobreescribir la distancia con la suma obtenida y guardar al nodo actual como predecesor del vecino
        4. Cuando se termina de revisar a todos los vecino del nodo actual, se marca como visitado y se elimina del conjunto no  visitado
        5. Continúa la ejecución hasta vaciar al conjunto no visitado
        6. Seleccionar el nodo no visitado con menor distancia tentativa y marcarlo como el nuevo nodo actual. Regresar al punto 3
        """
        if a in self.vertices:
            self.vertices[a].distancia = 0
            actual = a
            noVisitados = []

            for v in self.vertices:
                if v != a:
                    self.vertices[v].distancia = float('inf')
                self.vertices[v].padre = None
                noVisitados.append(v)

            while len(noVisitados) > 0:
                for vecino in self.vertices[actual].vecinos:
                    if self.vertices[vecino[0]].visitado == False:
                        if self.vertices[actual].distancia + vecino[1] < self.vertices[vecino[0]].distancia:
                            self.vertices[vecino[0]
                                          ].distancia = self.vertices[actual].distancia + vecino[1]
                            self.vertices[vecino[0]].padre = actual

                self.vertices[actual].visitado = True
                noVisitados.remove(actual)
                actual = self.minimo(noVisitados)
        else:
            return False


class main:
    g = Grafo()
    g.agregarVertice(1)
    g.agregarVertice(2)
    g.agregarVertice(3)
    g.agregarVertice(4)
    g.agregarVertice(5)
    g.agregarVertice(6)
    g.agregarArista(1, 6, 14)
    g.agregarArista(1, 2, 7)
    g.agregarArista(1, 3, 9)
    g.agregarArista(2, 3, 10)
    g.agregarArista(2, 4, 15)
    g.agregarArista(3, 4, 11)
    g.agregarArista(3, 6, 2)
    g.agregarArista(4, 5, 6)
    g.agregarArista(5, 6, 9)

    print("\n\nLa ruta mas rapida por Dijkstra junto con su distancia es:")
    g.dijkstra(1)
    print(g.camino(1, 6))
    print("\nLos valores finales del grafo son los siguientes:")
    g.imprimirGrafo()
