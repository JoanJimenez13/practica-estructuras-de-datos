class COLA(object):
	"""docstring for COLA"""
	def __init__(self):
		self.Inicial = None
		self.Final = None
		self.COLA = []

	def GetInicial():
		print (self.Inicial)
	
	def GetFinal():
		print (self.Final)

	def add(self, Elemento):
		self.COLA.append(Elemento)
		self.Final = Elemento

	def Remove(self):
		self.COLA.pop(0)
		self.Inicial = self.COLA[0]

	def _str_(self):
		print (str(self.COLA))
		
	def _len_(self):
		print (len (self.COLA))




MyCola = COLA()
#AÑADIENDO DATOS A LA COLA
MyCola.add(1)
MyCola.add(2)
MyCola.add(3)
MyCola.add(4)
MyCola.add(5)
print("ELEMENTOS DE LA COLA: ")
print (MyCola.COLA)


#REMOVIENDO DATOS DE LA COLA
print("REMOVIENDO DATOS DE LA COLA... ")
MyCola.Remove()
MyCola.Remove()
print("DATOS DE LA COLA ACTUALMENTE: ")		
print (MyCola.COLA)	